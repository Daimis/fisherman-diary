package com.app.daimis.fishermansdiary.presenters.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.app.daimis.fishermansdiary.data.entities.InfoFish;

public class InfoFishAdapter extends ArrayAdapter<InfoFish> {
    private Context context;
    private InfoFish[] infoFishes;

    public InfoFishAdapter(Context context, int res, InfoFish[] infoFishes) {
        super(context, res, infoFishes);
        this.context = context;
        this.infoFishes = infoFishes;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        /*if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.layout_content_chapter_row, parent, false);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.textChapter = (TextView) rowView.findViewById(R.id.textViewRowContent);
            viewHolder.textPageNumber = (TextView) rowView.findViewById(R.id.textViewRowContentPageNumber);
            rowView.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.textChapter.setText(bookChapters[position].getChapterName());
        holder.textPageNumber.setText(Integer.toString(bookChapters[position].getChapterStartPageNumber()+1));
*/
        return rowView;
    }

    static class ViewHolder {

    }
}
